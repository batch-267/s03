<?php require_once "./code.php";?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03: Classes and Objects</title>
</head>
<body>
	<h1>Objects from </h1>
	<p><?php var_dump($buildingObj);?></p>
	<p><?php echo $buildingObj->name;?></p>

	<h1>Objects from Classes</h1>
	<p><?php var_dump($building); ?></p>
	<p><?php echo $building->printName(); ?></p>

	<h2>Modifying the Instantiated Object</h2>
	<?php $building->name = "GMA Network";?>
	<!-- <?php $building->floors = "twenty";?> -->
	<p><?php var_dump($building); ?></p>
	<p><?php echo $building->printName(); ?></p>

	<h1>Inheritance (Condominium Object)</h1>
	<p><?php var_dump($condominium); ?></p>
	<p><?= $condominium->name; ?></p>
	<p><?= $condominium->floors; ?></p>
	<p><?= $condominium->address; ?></p>
	

	<h1>Polymorphism (Overidding the behavior of the printName() method</h1>
	<p><?= $condominium->printName(); ?></p>	
</body>
</html>