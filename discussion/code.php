<?php

//[SECTION] Objects as Variables

$buildingObj = (object)[
	"name" => "Caswynn Building",
	"floors" => 8,
	"address" => "Brgy. Sacred Heart, Quezon City, Philippines"
];

$buildingObj2 = (object)[
	"name" => "GMA Network",
	"floors" => 20,
	"address" => "Brgy. Sacred Heart, Quezon City, Philippines"
];


class Building{
	//Properties
	public $name;
	public $floors;
	public $address;

	//Constructor Function
	public function __construct($name, $floors, $address){
		/*
			$this accesses the current class
		*/
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
	//Methods
	/*
		these are functions inside an object that can perform a specific action
	*/
	public function printName(){
		return "The name of the building is $this->name";
	}
}

$building = new Building("Caswynn Building",8,"Timog Avenue, Quezion City, Philippines");


// [SECTION] Inheritance and Polymorphism

//Inheritance - The derived classes are allowed to inherit properties and methods from a specified base class

Class Condominium extends Building{
	//Polymorphism - methods inherited by the child class can be overidden to have a behaviour different from the method of the parent class

	public function printName(){
		return "The name of the condominium is $this->name";
	}
}

$condominium = new Condominium("Enzo Condo",5, "Buendia Avenue, Makati City, Philippines");
