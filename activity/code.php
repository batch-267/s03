<?php

class Person{

	public $firstName;
	public $middleName;
	public $lastName;

	function __construct($firstName, $middleName, $lastName){
		$this->fullName = $firstName . " " . $middleName . " " . $lastName;
	}

	public function printName(){
		return "Your full name is $this->fullName";
	}
}

$person = new Person("Sengku","","Ishigami");

class Developer extends Person{
	public function printName(){
		return "Your name is $this->fullName and you are a developer";
	}
}
$developer = new Developer("John","Finch","Smith");


class Engineer extends Person{
	public function printName(){
		return "You are an engineer named $this->fullName";
	}
}
$engineer = new Engineer("Harold", "Myers", "Reese");