<?php require_once "./code.php";?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: Activity</title>
</head>
<body>
	<h1>Person</h1>
	<?= $person->printName(); ?>

	<h1>Developer</h1>
	<?= $developer->printName(); ?>

	<h1>Engineer</h1>
	<?= $engineer->printName(); ?>

</body>
</html>